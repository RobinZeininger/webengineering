firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        // User is signed in.
        document.getElementById("headline-button").innerText = "Logout";
    } else {
        // No user is signed in.
        document.getElementById("headline-button").innerText = "Login";
    }
});

function login(){
    var emailInput = document.getElementById("emailInput").value;
    var pwInput = document.getElementById("pwInput").value;

    firebase.auth().signInWithEmailAndPassword(emailInput, pwInput).then(function(user) {
        // user signed in
        window.alert("Hi " + user.email);
        window.location.replace("content.html");
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;

        window.alert("Error : " + errorMessage);
    });
}