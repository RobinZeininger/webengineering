var user = firebase.auth().currentUser;

if (user) {
    // User is signed in.
} else {
    window.alert("You need to sign in again to see content");
    window.location.replace("login.html");
}

function logout(){
    window.alert("Signed out. You need to sign in again to see content");
    firebase.auth().signOut().then(function() {
        window.location.replace("login.html");
    }, function(error) {
        window.alert("Error : " + error);
    });
}